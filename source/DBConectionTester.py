#!/usr/bin/env python
# coding=utf-8

# A program to test MYSQL and MSSQL conection with a query and interval
# Copyright (C) 2016  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pygtk
import glib
import gtk
import MySQLdb
import pyodbc
import json #to read config file
from pprint import pprint
pygtk.require('2.0')

class DBconectionTester:

    textview = gtk.TextView()
    textbuffer = textview.get_buffer()
    config = {}
    interval = 500;
    state = False
    counter = 1;
    source_id = None;
    db = None;


    # Contructor
    def __init__(self):
        # Create a new window
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_size_request(400, 400)
        window.set_title("DBconectionTester")
        window.connect("delete_event", self.delete_event)
        window.connect("destroy", self.destroy)
        window.set_border_width(10)

        # Main VBox
        vbox = gtk.VBox(False, 0)
        window.add(vbox)

        # Label
        string = "DB User Passwd:"
        label = gtk.Label(string)
        vbox.pack_start(label, False, True, 0)

        # Entry
        self.entryPasswd = gtk.Entry()
        self.entryPasswd.set_visibility(False)
        vbox.pack_start(self.entryPasswd, False, False, 0)

        # Label
        string = "Time Delay (ms)"
        label = gtk.Label(string)
        vbox.pack_start(label, False, True, 0)

        # Entry
        self.entry_interval = gtk.Entry()
        vbox.pack_start(self.entry_interval, False, False, 0)

        # Btn
        button = gtk.Button("Update Interval")
        button.connect("clicked", self.update_interval)
        vbox.pack_start(button, False, False, 0)

        # Btn
        button = gtk.Button("Connect")
        button.connect("clicked", self.connect)
        vbox.pack_start(button, False, False, 0)

        # Btn
        button = gtk.Button("Start")
        button.connect("clicked", self.start)
        vbox.pack_start(button, False, False, 0)

        # Btn
        button = gtk.Button("Stop")
        button.connect("clicked", self.stop)
        vbox.pack_start(button, False, False, 0)

        string = "Output:"
        label = gtk.Label(string)
        vbox.pack_start(label, False, False, 0)

        # Textarea
        self.textview.set_editable(False)
        sw = gtk.ScrolledWindow()
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        sw.add(self.textview)
        vbox.pack_start(sw, True, True, 0)
        self.textview.set_cursor_visible(False)

        # Show all window
        window.show_all()

    # Gtk main loop
    def gtk_main(self):
        gtk.main()

    # Gtk events
    def delete_event(self, widget, event, data=None):
        return False

    def destroy(self, widget, data=None):
        gtk.main_quit()

    def console_log(self, message):
        end_iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(end_iter, message+"\n")
        return True

    def load_cfg_file(self):
        try:
            with open('config.json', 'r') as f:
                self.config = json.load(f)
                return True
        except EOFError:
            print "Error opening config.cfg file"
            return False

    def update_interval(self, data):
        interval = self.entry_interval.get_text();
        if not interval:
            self.console_log("Type an interval please.")
            return False
        if not interval.isdigit() or int(interval) <= 10:
            self.console_log("incorrect interval value.")
            return False
        if self.source_id is None:
            self.console_log("It isn't running, please start first.")
            return False
        glib.source_remove(self.source_id)
        self.interval = int(interval)
        self.start(True)
        return True

    def connect(self, data):
        # get config.json data
        self.load_cfg_file()
        self.config['passwd'] = self.entryPasswd.get_text()

        # conecting to db
        if self.config['driver'] == 'mysql':
            self.db = MySQLdb.connect(host = self.config['host'],
                                      user = self.config['user'],
                                      passwd = self.config['passwd'],
                                      db = self.config['db'])

        elif self.config['driver'] == 'mssql':
            conectionString = 'DRIVER={SQL Server};SERVER=' + self.config['host'] + ';DATABASE=' + self.config['db'] + ';UID=' + self.config['user'] + ';PWD=' + self.config['passwd']+';'
            self.db = pyodbc.connect('DRIVER={SQL Server};SERVER=' + self.config['host'] + ';DATABASE=' + self.config['db'] + ';UID=' + self.config['user'] + ';PWD=' + self.config['passwd'] + '')

        else:
            self.console_log("Driver unknown, verify config.json")

        if self.db is not None:
            self.console_log("*********** Conection OK Database: %s ***********"% self.config['db'])
            message = "-> \tDriver: %s\t Host: %s\t Table: %s "% (self.config['driver'], self.config['host'], self.config['db'])
            self.console_log(message)
            self.console_log("*****************************************************")
        return True

    def do_query(self):
        self.console_log("--------------------- Query execution n° %d "%self.counter)
        self.counter += 1
        cur = self.db.cursor()
        # Use all the SQL you like
        cur.execute(self.config['query'])
        if self.config['commit']:
            self.db.commit()

        # print all the first cell of all the rows
        if self.config['fetchall']:
            for row in cur.fetchall():
             self.console_log("Result: %s"%row[0])
        return True

    def start(self, data):
        if self.db is None:
            self.console_log("Connect first.")
            return False
        self.counter = 0
        self.source_id = glib.timeout_add(self.interval, self.do_query)

    def stop(self, data):
        if self.source_id is None:
            self.console_log("It isn't running, please start first.")
            return False

        self.console_log("************************ STOPED *****************************")
        glib.source_remove(self.source_id)
        self.source_id = None
        self.db.close()
        self.db = None

# Main
def main():
    app = DBconectionTester()
    app.gtk_main()

if __name__ == '__main__':
    # Call to main
    main()