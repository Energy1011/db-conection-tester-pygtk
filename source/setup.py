from distutils.core import setup
import py2exe
import sys

# Find GTK+ installation path
__import__('gtk')
m = sys.modules['gtk']
gtk_base_path = m.__path__[0]

setup(
    name = 'DBConectiontester',
    description = 'DBConectiontester',
    version = '1.0',

    windows = [
                  {
                      'script': 'DBConectiontester.py'
                  }
              ],

    options = {
                  'py2exe': {
                      'packages':'encodings',
                      # Optionally omit gio, gtk.keysyms, and/or rsvg if you're not using them
                      'includes': 'cairo, pango, pangocairo, atk, gobject, gio, gtk.keysyms, rsvg',
                  }
              }
)